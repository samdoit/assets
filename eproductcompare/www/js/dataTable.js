/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseInt( $('#min').val(), 10 );
        var max = parseInt( $('#max').val(), 10 );
        var age = parseFloat( data[3] ) || 0; // use data for the age column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age   && isNaN( max ) ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
);

$(document).ready(function() {
    var table = $('#dataTable-Visitor').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "ajax/trackers",
            "data": function ( d ) {
                d.myKey = "myValue";
                d.custom = "Arul";//$('#myInput').val();
                // etc
            }
        },
        "columns": [
                { "data" : "id",          "name" : "id",		"title" : "Id", "orderable": true },
		{ "data" : "client_ip",   "name":"client_ip",		"title" : "IP Address", "orderable": true },
                { "data" : "country",     "name" : "country",		"title" : "Country / City", "orderable": true },
                { "data" : "user",        "name" : "user",		"title" : "User", "orderable": true },
                { "data" : "device",      "name" : "device",		"title" : "Device", "orderable": true },
                { "data" : "browser",     "name" : "browser",		"title" : "Browser", "orderable": true },
                { "data" : "referer",     "name" : "referer",		"title" : "Referer", "orderable": true },
                { "data" : "pageViews",   "name" : "pageViews",		"title" : "Page Views", "orderable": true },
                { "data" : "lastActivity","name" : "lastActivity",	"title" : "Last Activity", "orderable": true }
        ]
    } );

    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').keyup( function() {
        table.draw();
console.log("Manickam");
    } );

    $('#dataTable-Bot').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "ajax/trackers/bot",
        "columns": [
                { "data" : "id",          "name" : "id",                "title" : "Id", "orderable": true },
                { "data" : "client_ip",   "name":"client_ip",           "title" : "IP Address", "orderable": true },
                { "data" : "country",     "name" : "country",           "title" : "Country / City", "orderable": true },
                { "data" : "user",        "name" : "user",              "title" : "User", "orderable": true },
                { "data" : "device",      "name" : "device",            "title" : "Device", "orderable": true },
                { "data" : "browser",     "name" : "browser",           "title" : "Browser", "orderable": true },
                { "data" : "referer",     "name" : "referer",           "title" : "Referer", "orderable": true },
                { "data" : "pageViews",   "name" : "pageViews",         "title" : "Page Views", "orderable": true },
                { "data" : "lastActivity","name" : "lastActivity",      "title" : "Last Activity", "orderable": true }
        ]
    } );

});
